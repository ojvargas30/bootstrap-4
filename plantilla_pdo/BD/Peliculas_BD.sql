-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-05-2020 a las 22:56:09
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.1

-- DROP DATABASE peliculas;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `php`
--

CREATE DATABASE peliculas;
USE peliculas;
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id_PK` int(11) NOT NULL,
  `estado_id_FK` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id_PK`, `estado_id_FK`, `nombre`) VALUES
(1, 1, 'Acción'),
(2, 1, 'aventuras'),
(3, 1, 'Comedias'),
(4, 1, 'Dramáticas'),
(5, 1, 'terror'),
(6, 1, 'Musicales'),
(7, 1, 'Ciencia ficción'),
(8, 1, 'guerra o bélicas'),
(9, 1, 'Adultos'),
(10, 1, 'Infantiles');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_pelicula`
--

CREATE TABLE `categoria_pelicula` (
  `id_PK` int(11) NOT NULL,
  `pelicula_id_FK` int(11) NOT NULL,
  `categoria_id_FK` int(11) NOT NULL,
  `estado_id_FK` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categoria_pelicula`
--

INSERT INTO `categoria_pelicula` (`id_PK`, `pelicula_id_FK`, `categoria_id_FK`, `estado_id_FK`) VALUES
(1, 1, 1, 1),
(2, 2, 2, 1),
(3, 3, 3, 1),
(4, 4, 4, 1),
(5, 5, 5, 1),
(6, 6, 6, 1),
(7, 7, 7, 1),
(8, 8, 8, 1),
(9, 9, 9, 1),
(10, 10, 10, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `peliculas`
--

CREATE TABLE `peliculas` (
  `id_PK` int(11) NOT NULL,
  `estado_id_FK` int(11) NOT NULL,
  `usuario_id_FK` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `descripcion` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `peliculas`
--

INSERT INTO `peliculas` (`id_PK`, `estado_id_FK`, `usuario_id_FK`, `nombre`, `descripcion`) VALUES
(1, 1, 1, 'The Warriors, los amos de la noche The Warriors', 'Dirección: Walter Hill\nReparto: Michael Beck, David Harris, James Remar, Deborah Van Valkenburgh, Thomas G. Waites, Dorsey Wright\nDe entre el nutrido listado de notables filmes de acción que engrosan la filmografía de Walter Hill me veo \nobligado a quedarme con esta The Warriors. Una épica urbana ambientada en una Nueva York dominada por las \nbandas que bebe de la Grecia clásica y que a día de hoy continúa siendo tan fresca, icónica y sorprendente \ncomo el primer día. Un pequeño largometraje reconvertido en clásico de culto por méritos propios.'),
(2, 1, 2, '1997: Rescate en Nueva York', 'Dirección: John Carpenter\nReparto: Kurt Russell, Lee Van Cleef, Ernest Borgnine, Donald Pleasence, Season Hubley, Isaac Hayes\nEl maestro John Howard Carpenter tan pronto te dirige un clásico imperecedero del cine de terror como \nHalloween como se saca de la manga una película de acción distópica de la talla de Rescate en Nueva York.\n Una clase magistral de economía de recursos, con un uso ejemplar de la urgencia en el guión y un protagonista \n irrepetible cuya imagen y chulería le han hecho trascender como icono popular'),
(3, 1, 3, 'Golpe en la pequeña China', 'Dirección: John Carpenter\nReparto: Kurt Russell, Kim Cattrall, Dennis Dun, Kate Burton, Victor Wong, James Hong\nno sólo demostró la buena mano de John Carpenter para abordar el género de la acción; también \nprobó la versatilidad del director —y que sus colaboraciones con Kurt Russell son oro puro—. En esta\n ocasión, el master of horror nos deleitó con una comedia de acción ligera y libre de toda gravedad \n que abrazaba sin complejos los clichés del cine clásico de artes marciales con un toque de fantasía \n delicioso. Gloria ochentera en estado puro.'),
(4, 1, 4, 'Arma Letal', 'Dirección: Richard Donner\nReparto: Mel Gibson, Danny Glover, Gary Busey, Mitchell Ryan, Tom Atkins, Darlene Love\nSi bien fue la genial de Walter Hill la que popularizó el subgénero de la buddy cop pelicula, \nlo elevó al Olimpo juntando a dos genios como el director Richard Donner y el guionista Shane\n Black. Sumando a la ecuación a la pareja compuesta por Mel Gibson y Danny Glover en los papeles\n de los míticos agentes Riggs y Murtaugh, y un balance casi perfecto entre comedia, drama y acción,\n el resultado sólo puede ser una de las mejores cintas que ha dado la historia del género.'),
(5, 1, 5, 'Jungla de Cristal', 'Dirección: John McTiernan\nReparto: Bruce Willis, Bonnie Bedelia, Alan Rickman, Alexander Godunov, Reginald Veljohnson, Paul Gleason\nTodas las palabras que puedan escribirse para ensalzar esta auténtica maravilla son pocas. John McTiernan,\n con su tercer largometraje, no sólo tocó el techo de su carrera —con todo respeto que dio a luz el que, \n para el que suscribe, es el mejor filme de acción de todos los tiempos. Espectacular, divertidísima, \n violenta, con un antagonista de órdago y con un Bruce Willis irrepetible en su papel como John McClane.\n Yippee Ki yay!'),
(6, 1, 6, 'Robocop', 'Dirección: Paul Verhoeven\nReparto: Peter Weller, Nancy Allen, Kurtwood Smith, Miguel Ferrer, Ronny Cox, Dan O\n\"Mitad hombre, mitad máquina, todo policía. Con esta tagline coronando su póster, es imposible que\n Robocop decepcione a nadie. No sólo es una de las películas más violentas de esta lista —la escena \n en la que masacran al pobre Murphy sigue siendo espeluznante—; sino que atesora una dirección brillante \n de un Paul Verhoeven cuyo debut en Estados Unidos marcó un antes y un después en su carrera.'),
(7, 1, 7, 'Desafío Total', 'Dirección: Paul Verhoeven\nReparto: Arnold Schwarzenegger, Sharon Stone, Michael Ironside, Rachel Ticotin, Ronny Cox, Marshall Bell\nBuena muestra de la herencia que Robocop a la carrera en la acción de Paul Verhoeven se ve reflejada en \nesta genial adaptación del relato Podemos recordarlo todo por usted de Philip K. Dick. Su ambientación\n marciana es extraordinaria, al igual que su apabullante diseño de producción; aunque si por algo la \n recordaremos es por el protagonismo de un Arnold Schwarzenegger particularmente inspirado.'),
(8, 1, 8, 'Commando', 'Dirección: Mark L. Lester\nReparto: Arnold Schwarzenegger, Rae Dawn Chong, Dan Hedaya, James Olson, Bill Duke, Vernon Wells\nOjo, porque la participación del bueno de Arnie en esta lista no se limita a un único título —ni a\n dos—; y es que el ex \"Governator\" nos ha regalado perlas como esta Commando: una película de \n acción ochentera canónica y testosterónica, con un protagonista con un nombre tan molón como John\n Matrix y unos músculos a punto de explotar, un duelo final cuerpo a cuerpo y una tonelada de frases\n lapidarias de la talla de \"Como boinas verdes para desayunar, y ahora mismo estoy hambriento\". \n Ahí es nada.'),
(9, 1, 9, 'Depredador', 'Dirección: John McTiernan\nReparto: Arnold Schwarzenegger, Carl Weathers, Sonny Landham, Bill Duke, Elpidia Carrillo, Richard Chaves\nSiguiendo con el \"Chuache\", cómo olvidar el berrido que pega, antorcha en mano, para avisar al \nDepredador de que tiene las horas contadas. John McTiernan demostrando una vez más por qué, con\n tan sólo 12 títulos a sus espaldas, es uno de los grandes directores de acción. De nuevo,\n testosterona a espuertas en una curiosa aproximación a la ciencia ficción que no ha conseguido \n ser superada por ninguna de sus estimables secuelas.'),
(10, 1, 10, 'Mentiras arriesgadas', 'Dirección: James Cameron\nReparto: Arnold Schwarzenegger, Jamie Lee Curtis, Tom Arnold, Charlton Heston, Art Malik, Bill Paxton\nNo son pocas las voces que afirman que el cine de acción de esencia más ochentera murió en \n1994 de la mano de James Cameron con la genial Mentiras arriesgadas. Artesanía pura y dura\n —la secuencia del puente continúa siendo uno de los puntos clave de la historia del género—,\n violencia, emoción, un tono ligero y una química tremebunda entre Schwarzenegger y Jamie Lee\n Curtis. ¡Ah! Y un strip-tease que ha pasado a la historia del séptimo arte, que no se nos\n olvide.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `id_PK` int(11) NOT NULL,
  `estados` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`id_PK`, `estados`) VALUES
(1, b'1'),
(2, b'0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_PK` int(11) NOT NULL,
  `estado_id_FK` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `email` varchar(200) NOT NULL,
  `pass` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_PK`, `estado_id_FK`, `nombre`, `email`, `pass`) VALUES
(1, 1, 'Arturo Calle', 'borak@skirting.edu', 'asdu8y3'),
(2, 1, 'Arturo Jose', 'boraak@skirting.edu', 'asdu8ysadaa3'),
(3, 1, 'Andres Calle', 'borsk@skirting.edu', 'asduaazzzsdsa8y3'),
(4, 1, 'Alex Calle', 'borazz@skirting.edu', 'asdu8asasdy3'),
(5, 1, 'Maria Calle', 'boraxk@skirting.edu', 'asdu8asazzsdy3'),
(6, 1, 'Zen Calle', 'borakxazz@skirting.edu', 'asduasx8y3'),
(7, 1, 'Ebri Calle', 'boaraaaak@skirting.edu', 'asdxcxxxu8y3'),
(8, 1, 'Arturo boni', 'boaaaarak@skirting.edu', 'asduzxczx8y3'),
(9, 1, 'Ed Calle', 'boraaaka@skirting.edu', 'asdu8zxczxcy3'),
(10, 1, 'Juan Calle', 'baaorak@skirting.edu', 'asduzzz8y3');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_PK`),
  ADD KEY `estado_id_FK` (`estado_id_FK`);

--
-- Indices de la tabla `categoria_pelicula`
--
ALTER TABLE `categoria_pelicula`
  ADD PRIMARY KEY (`id_PK`),
  ADD KEY `estado_id_FK` (`estado_id_FK`),
  ADD KEY `pelicula_id_FK` (`pelicula_id_FK`),
  ADD KEY `categoria_id_FK` (`categoria_id_FK`);

--
-- Indices de la tabla `peliculas`
--
ALTER TABLE `peliculas`
  ADD PRIMARY KEY (`id_PK`),
  ADD KEY `estado_id_FK` (`estado_id_FK`),
  ADD KEY `usuario_id_FK` (`usuario_id_FK`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`id_PK`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_PK`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `estado_id_FK` (`estado_id_FK`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_PK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `categoria_pelicula`
--
ALTER TABLE `categoria_pelicula`
  MODIFY `id_PK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `peliculas`
--
ALTER TABLE `peliculas`
  MODIFY `id_PK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `estado`
--
ALTER TABLE `estado`
  MODIFY `id_PK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_PK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD CONSTRAINT `categoria_ibfk_1` FOREIGN KEY (`estado_id_FK`) REFERENCES `estado` (`id_PK`);

--
-- Filtros para la tabla `categoria_pelicula`
--
ALTER TABLE `categoria_pelicula`
  ADD CONSTRAINT `categoria_pelicula_ibfk_1` FOREIGN KEY (`estado_id_FK`) REFERENCES `estado` (`id_PK`),
  ADD CONSTRAINT `categoria_pelicula_ibfk_2` FOREIGN KEY (`pelicula_id_FK`) REFERENCES `peliculas` (`id_PK`),
  ADD CONSTRAINT `categoria_pelicula_ibfk_3` FOREIGN KEY (`categoria_id_FK`) REFERENCES `categoria` (`id_PK`);

--
-- Filtros para la tabla `peliculas`
--
ALTER TABLE `peliculas`
  ADD CONSTRAINT `peliculas_ibfk_1` FOREIGN KEY (`estado_id_FK`) REFERENCES `estado` (`id_PK`),
  ADD CONSTRAINT `peliculas_ibfk_2` FOREIGN KEY (`usuario_id_FK`) REFERENCES `usuarios` (`id_PK`);

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`estado_id_FK`) REFERENCES `estado` (`id_PK`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
